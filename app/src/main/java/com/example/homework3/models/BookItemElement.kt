package com.example.homework3.models

data class BookItemElement(
    var id: Int,
    var title: String?,
    var author: String?,
    var description: String?
)