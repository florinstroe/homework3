package com.example.homework3.interfaces;

import com.example.homework3.models.BookItemElement;

public interface OnItemClickListener {
    void onBookClick(BookItemElement book);

    void onBookDeleteClick(BookItemElement book);
}
