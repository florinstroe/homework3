package com.example.homework3.interfaces;

import com.example.homework3.models.BookItemElement;

public interface ActivityFragmentCommunication {
    void addMainFragment();

    void addBookFragment(BookItemElement book);
}
