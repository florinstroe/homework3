package com.example.homework3.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.example.homework3.R
import com.example.homework3.fragments.BookFragment
import com.example.homework3.fragments.MainFragment
import com.example.homework3.interfaces.ActivityFragmentCommunication
import com.example.homework3.models.BookItemElement


class MainActivity : AppCompatActivity(), ActivityFragmentCommunication {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        addMainFragment()
    }

    override fun addMainFragment() {
        val fragmentManager: FragmentManager = supportFragmentManager
        val transaction: FragmentTransaction = fragmentManager.beginTransaction()
        val tag: String = MainFragment::class.java.getName()
        val addTransaction: FragmentTransaction = transaction.add(
            R.id.frame_layout, MainFragment(), tag
        )
        addTransaction.commit()
    }

    override fun addBookFragment(book: BookItemElement?) {
        val fragmentManager: FragmentManager = supportFragmentManager
        val transaction: FragmentTransaction = fragmentManager.beginTransaction()
        val tag: String = BookFragment::class.java.getName()
        val replaceTransaction: FragmentTransaction = transaction.replace(
            R.id.frame_layout, BookFragment.newInstance(book), tag
        )
        replaceTransaction.addToBackStack(tag)
        replaceTransaction.commit()
    }
}